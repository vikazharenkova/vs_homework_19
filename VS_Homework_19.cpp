﻿// VS_Homework_19.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class Animal
{
private:
    std::string animal;
public:
    Animal() 
    {}
    Animal(std::string _animal) : animal(_animal)
    {}

    virtual void Voice()
    {
        std::string voice;
        std::cout << "What does the " << animal << " say?" << std::endl;
        std::cin >> voice;
        std::cout << animal << " says " << voice << std::endl;
    }
 };

class Dog : public Animal
{
private:
    std::string dog;
public:
    Dog() : dog("Dog"), Animal("Dog")
    {}

    void Voice() override
    {
        std::cout << dog << " says 'Woof'" << std::endl;
    }
};

class Cat : public Animal
{
private:
    std::string cat;
public:
    Cat() : cat("Cat"), Animal("Cat")
    {}

    void Voice() override
    {
        std::cout << cat << " says 'Meow'" << std::endl;
    }
};

class Mouse : public Animal
{
private:
    std::string mouse;
public:
    Mouse() : mouse("Mouse"), Animal("Mouse")
    {}

    void Voice() override
    {
        std::cout << mouse << " says 'Squeek'" << std::endl;
    }
};

class Bird: public Animal
{
private:
    std::string bird;
public:
    Bird() : bird("Bird"), Animal("Bird")
    {}

    void Voice() override
    {
        std::cout << bird << " says 'Tweet'" << std::endl;
    }
};

int main()
{
    Animal* animals[]{ new Dog(), new Cat(), new Mouse(), new Bird() };
   
    for (int i = 0; i < 4; i++)
    {
        animals[i]->Voice();
    }

    return 0;  
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
